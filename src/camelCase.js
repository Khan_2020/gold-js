/**
 * This function turns the specified string into camel cased identifier. Camel case (stylized
 * as camelCase; also known as camel caps or more formally as medial capitals) is the practice
 * of writing phrases such that each word or abbreviation in the middle of the phrase begins
 * with a capital letter, with no intervening spaces or punctuation. For example.
 *
 * - `safe hTML` -> `safeHtml`
 * - `escape HTML entities` -> `escapeHtmlEntities`
 *
 * The identifier should only contains english letters (`A` to `Z`, including upper and
 * lower case), digits (`0` to `9`) and underscore (`_`). Other characters will be treated
 * as delimiters. For example.
 *
 * - `safe+html` -> `safeHtml`
 *
 * @param {String} string The input string.
 */
export default function camelCase (string) {
  // TODO:
  //   Please implement the function.
  // <-start-
  if (string === '' || string === undefined) return;
  if (string == null) return null;

  const newArray = string.split(/[+-/*/$ ]/);

  let camelString = newArray[0].toLowerCase();
  for (let i = 1; i < newArray.length; i++) {
    camelString += newArray[i].substr(0, 1).toUpperCase();
    camelString += newArray[i].substr(1, newArray[i].length - 1).toLowerCase();
  }

  return camelString;
  // --end-->
}

// TODO:
//   You can add additional code here if you want
// <-start-

// --end-->
